package nl.kzaconnected.cursus.service;

import nl.kzaconnected.cursus.configuration.ResourceNotFoundException;
import nl.kzaconnected.cursus.model.Dao.*;
import nl.kzaconnected.cursus.model.Dto.CursusDto;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class CursusService extends AbstractCursusService {

    public CursusDto findOne(Long id) {
        Cursus cursus = cursusRepository.findById(id)
                .orElseThrow(ResourceNotFoundException::new);
        CursusDto cursusDto = convertToDto(cursus);
        cursusDto.getCursusdata().sort(Comparator.comparing(Datum::getDatum));
        return cursusDto;
    }

    public List<CursusDto> findAll() {
        List<Cursus> cursussen = cursusRepository.findAll();
        List<CursusDto> cursusDtos = convertToDtoList(cursussen);
        for (CursusDto cursusDto : cursusDtos) {
            cursusDto.getCursusdata().sort(Comparator.comparing(Datum::getDatum));
        }
        cursusDtos.sort(Comparator.comparing(c -> c.getCursusdata().get(0).getDatum()));
        return cursusDtos;
    }

    public CursusDto save(CursusDto cursusDto) throws IllegalArgumentException {
        Cursus cursus = convertToDao(cursusDto);

        // name of cursus shoud not be longer then 32
        if (cursusDto.getNaam().length() > 32) {
            throw new IllegalArgumentException("Cursus naam is te lang");
        }

        // attitude should contain following data:
        List<String> attitudeList = new ArrayList<>();
        attitudeList.add("Test attitude");
        attitudeList.add("Technical attitude");
        attitudeList.add("Business attitude");
        if (!attitudeList.contains(cursus.getAttitude().getAttitude())) {
            throw new IllegalArgumentException("Attitude bestaat niet");
        }

        // slagingscriterium should contain following data:
        List<String> slagingscriteriumList = new ArrayList<>();
        slagingscriteriumList.add("Cursusavonden gevolgd");
        slagingscriteriumList.add("Certificaat behaald");
        slagingscriteriumList.add("Zelfstudie");
        slagingscriteriumList.add("Praktijkervaring opgedaan");
        if (!slagingscriteriumList.contains(cursus.getSlagingscriterium().getSlagingscriterium())) {
            throw new IllegalArgumentException("Slagingscriterium bestaat niet");
        }

        // Status should contain following data:
        List<String> statusList = new ArrayList<>();
        statusList.add("Nog in te plannen");
        statusList.add("Inschrijving geopend");
        statusList.add("Inschrijving gesloten");
        if (!statusList.contains(cursus.getStatus().getStatus())) {
            throw new IllegalArgumentException("Status bestaat niet");
        }

        // Functieniveau should contain following data:
        List<String> functieniveauList = new ArrayList<>();
        functieniveauList.add("Trainee");
        functieniveauList.add("1");
        functieniveauList.add("2");
        functieniveauList.add("3");
        if (!functieniveauList.contains(cursus.getFunctieniveau().getFunctieniveau())) {
            throw new IllegalArgumentException("Functieniveau bestaat niet");
        }

        // Cursus docent should always contain a docent
        // A docent naam should not be longer than 32
        // A docent email should be valid
        if (cursus.getCursusdocenten().size() == 0) {
            throw new IllegalArgumentException("Docenten moet gevuld zijn");
        } else {
            for (Docent docent : cursus.getCursusdocenten()) {
                if (docent.getNaam().length() > 32) {
                    throw new IllegalArgumentException("Docenten naam mag niet langer zijn dan 32 karakters");
                }
                if (!EmailValidator.getInstance().isValid(docent.getEmail())) {
                    throw new IllegalArgumentException("Docenten email moet valide zijn");
                }
            }
        }

        // If cursist exist:
        // A cursist naam should not be longer than 32
        // A cursist email should be valid
        if (cursus.getCursuscursisten() != null && !cursus.getCursuscursisten().isEmpty()) {
            for (Cursist cursist : cursus.getCursuscursisten()) {
                if (cursist.getNaam().length() > 32) {
                    throw new IllegalArgumentException("Cursist naam mag niet langer zijn dan 32 karakters");
                }
                if (!EmailValidator.getInstance().isValid(cursist.getEmail())) {
                    throw new IllegalArgumentException("Cursist email moet valide zijn");
                }
            }
        }

        Attitude attitude = attitudeRepository.findByAttitude(cursusDto.getAttitude());
        Functieniveau functieniveau = functieniveauRepository.findByFunctieniveau(cursusDto.getFunctieniveau());
        Slagingscriterium slagingscriterium = slagingscriteriumRepository.findBySlagingscriterium(cursusDto.getSlagingscriterium());
        Status status = statusRepository.findByStatus(cursusDto.getStatus());
        cursus.getAttitude().setId(attitude.getId());
        cursus.getFunctieniveau().setId(functieniveau.getId());
        cursus.getSlagingscriterium().setId(slagingscriterium.getId());
        cursus.getStatus().setId(status.getId());

        cursus = cursusRepository.save(cursus);
        return convertToDto(cursus);
    }

    public void delete(Long id) {
        cursusRepository.delete(
                cursusRepository.findById(id)
                        .orElseThrow(ResourceNotFoundException::new));
    }
}
